$(document).ready(function(){
    console.log("jQuery działa.");
    


    $("#menu").css("height", "100px");
    $("#content").css("height", (window.innerHeight-100) + "px");
    
    $('head').append('<link href="https://fonts.googleapis.com/css?family=Nosifer" rel="stylesheet">');
    $('head').append('<link rel="stylesheet" type="text/css" href="style.css">');

    $('html').append('<body></body>');

    $('body').append('<div id="menu"></div>');
        $('#menu').append('<div class="button" id="startButton">Start</div>');
        $('#menu').append('<div class="button" id="stopButton">Stop</div>');
        $('#menu').append('<div class="button" id="resetButton">Zresetuj</div>');
        $('#menu').append('<div id="punkty"><div id="ilepkt">0</div> punktów | <div id="poziom">1</div> poziom</div>');

    $('body').append('<div id="content"></div>');
        $('#content').append('<img src="bunny.png" id="bunny">');
    
    $("#startButton").click(function(){
        start();
    });
    $("#stopButton").click(function(){
        stop();
    });
    $("#resetButton").click(function(){
        reset();
    });
    $("#bunny").click(function(){
        trafiony();
    });
});
function trafiony(){
    temp = $("#ilepkt").text();
    temp = Number(temp);
    temp += 1;
    $("#ilepkt").text(temp);
    if((temp%10)==0){
        clearInterval(jazda);
        czas-=200;
        jazda = setInterval(function(){
            zmiana();
            }, czas);
        temp2 = $("#poziom").text();
        temp2 = Number(temp2);
        temp2 += 1;
        $("#poziom").text(temp2);
    }
    $("#bunny").css("display", "none");
    backgroundMaker();
}

function rand( min, max ){
    min = parseInt( min, 10 );
    max = parseInt( max, 10 );

    if ( min > max ){
        var tmp = min;
        min = max;
        max = tmp;
    }

    return Math.floor( Math.random() * ( max - min + 1 ) + min );
}

function zmiana(){
    $("#bunny").css("display", "inline-table");
    var x = rand(0, (window.innerWidth-100));
        
    var y = rand(0, (window.innerHeight-200));
    console.log("X" + x);
    
    $("#bunny").css("left", x + "px");
    $("#bunny").css("top", y + "px");
}
czas = 1500;
function start(){
    jazda = setInterval(function(){
        zmiana();
        }, 1500);
    
}

function stop(){
    clearInterval(jazda);
    
}

function reset(){
    clearInterval(jazda);
    $("#ilepkt").text("0");
    $("#poziom").text("1");
    czas=1500;
}

function backgroundMaker(){
    var kolory = ['black', 'yellow', 'red', 'purple', 'pink', 'brown', 'green', 'gray'];
    var koloryLength = kolory.length;

    var losowanie = rand(0, (koloryLength-1));

    $('body').css({"background-color":kolory[losowanie], "transition":"background-color 0.5s ease"});
}